# WPCLP #
## WPC / WPC95 Lamp Matrix Driver Rom Patcher ##

This program will update the lamp matrix driver code for WPC &
WPC95 game roms to eliminate 'ghosting' when using LEDs.

View [README.TXT](https://bitbucket.org/ericconnerapps/wpclp/src/master/README.TXT) for more info.